package main

import (
	"awesomeProject/client/users/org/b202/users"
	"context"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"log"
	"strconv"
	"strings"
	"time"
)

type Test interface {
	supports(name string) bool
	run(conn *grpc.ClientConn, channel chan bool)
}

type TestLogic func(conn *grpc.ClientConn, channel chan bool)

type TestImpl struct {
	name  string
	logic TestLogic
}

func (t *TestImpl) supports(name string) bool {
	return name == t.name
}

func (t *TestImpl) run(conn *grpc.ClientConn, channel chan bool) {
	t.logic(conn, channel)
}

func main() {
	testName, address, count := parseArgs()

	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatal("Failed to create connection", err)
	}
	defer conn.Close()

	tests := []TestImpl{{
		name: "usersAuth",
		logic: func(conn *grpc.ClientConn, channel chan bool) {
			defer func() { channel <- true }()
			client := users.NewAuthServiceClient(conn)
			signIn(client)
		},
	}, {
		name: "usersPublicKey",
		logic: func(conn *grpc.ClientConn, channel chan bool) {
			defer func() { channel <- true }()
			publicKeyClient := users.NewPublicKeyServiceClient(conn)
			key, err := publicKeyClient.GetPublicKey(context.Background(), &users.Void{})
			if err != nil {
				fmt.Println("Failed to request public key", err)
			}
			data, err := key.Recv()
			if err != nil {
				fmt.Println("Failed to get public key", err)
			}
		},
	}, {
		name: "usersRefreshToken",
		logic: func(conn *grpc.ClientConn, channel chan bool) {
			defer func() { channel <- true }()
			client := users.NewAuthServiceClient(conn)
			jwtPair := signIn(client)
			ctx := metadata.AppendToOutgoingContext(
				context.Background(), "Authorization", "Bearer "+jwtPair.RefreshToken)
			_, err = client.Refresh(ctx, &users.Void{})
			if err != nil {
				fmt.Println("Failed to refresh tokens", err)
			}
		},
	}}

	for _, test := range tests {
		if test.supports(testName) {
			channel := make(chan bool)
			defer close(channel)
			var readyCount int
			started := time.Now()
			for i := 0; i < count; i++ {
				go test.run(conn, channel)
			}
			fmt.Println("Waiting...")
			for true {
				if <-channel {
					readyCount++
					percents := readyCount * 100 / count
					fmt.Printf("\r[%s] %s%d %%", strings.Repeat("█", percents/2), strings.Repeat(" ", 50-percents/2), percents)
					if percents == 100 {
						fmt.Printf("\nCompleted, took %v ms\n", time.Now().Sub(started).Milliseconds())
						break
					}
				}
			}
			break
		}
	}

}

func signIn(client users.AuthServiceClient) *users.JwtPair {
	jwtPair, err := client.SignIn(context.Background(), &users.Credentials{
		Username: "admin",
		Password: "admin",
	})
	if err != nil {
		panic("Failed to signin")
	}
	return jwtPair
}

func parseArgs() (string, string, int) {
	flag.Parse()
	testName := flag.Arg(0)
	if testName == "" {
		log.Fatal("test name arg not specified")
	}
	address := flag.Arg(1)
	if address == "" {
		log.Fatal("address arg not specified")
	}
	count, err := strconv.Atoi(flag.Arg(2))
	if err != nil {
		log.Fatal("Invalid count arg")
	}
	return testName, address, count
}
